var rN1 = '#n1',
    rN2 = '#n2(_?\\d*)',
    rN3 = '#n3(_?\\d*)',
    rN4 = '#n4(_?\\d*)',
    rN5 = '#n5(_?\\d*)';

var onDeviceReady = function(){
    Router.set_routes([
        [rN1, n1Page],
        [rN2, n2Page],
        [rN3, n3Page],
        [rN4, n4Page],
        [rN5, n5Page]
    ]);

    Router.set_position([
        [rN1, {leftOf: [rN2,rN3,rN4,rN5]}],
        [rN2, {leftOf: [rN3,rN4,rN5], rightOf: [rN1]}],
        [rN3, {leftOf: [rN4,rN5], rightOf: [rN1,rN2]}],
        [rN4, {leftOf: [rN5], rightOf: [rN1,rN2,rN3]}],
        [rN5, {rightOf: [rN1,rN2,rN3,rN4]}]
    ]);

    Router.set_ignore("^#http(:?%3A|\:)");
    Router.check_url();

    document.addEventListener(START_EV, function(){tojump = true;}, false);
    document.addEventListener(MOVE_EV, function(){tojump = false;}, false);

    $(document).on(END_EV, '.link', function(){ if(tojump) {
        tojump = false;
        var hash = $(this).attr('rel');
        if (hash) location.hash = hash;
    }});

    $(document).on(END_EV,'.backbtn',function(){
        if(!tojump) return false;
        Router.get_back_from(location.hash);
    });

    $(document).on(END_EV,function(){
        hidebmenu();
        hidemenu();
    });
    
    //键盘动作监听
    document.addEventListener('keydown', function(e){
        if(e.keyCode===27){
            togglebmenu();
            return false;
        }
    }, false);
    
    //视当前手机数据，决定默认页面
    location.hash='#n1';
    hideloading();
};

function hide_tab(){
    $('#footer').hide();
    $('#content').css('bottom','0');
}
function show_tab(){
    $('#footer').show();
    $('#content').css('bottom','50px');
}

function n1Page(u){
    //菜单变动
    $('#footer_nav li').each(function(){
        this.className = (this.className.substr(0,6)=='nav_n1')?'nav_n1_now link':this.className.replace('_now','');
    });
    //菜单是否显示/二级菜单隐藏
    show_tab();
    //读取模板
    var html = $.tmpl('#tmpl_main');
    //新页面滑入
    slide_in(html, rN1, '.frame', function(){
        //滑入完成后的处理
        //开始联网获取数据
        mainScroller = createIScroll('#mainbodycontainer',function(){
            //tr('刷新')
            showloading();
            do_timeout(function(){hideloading();},2000);
        },function(){
            //tr('加载更多')
            showloading();
            do_timeout(function(){hideloading();},2000);
        },{mouseWheel:true,scrollbars:true,useTransition:false});
    });
}

function n2Page(u){
    $('#footer_nav li').each(function(){
        this.className = (this.className.substr(0,6)=='nav_n2')?'nav_n2_now link':this.className.replace('_now','');
    });
    show_tab();
    var html = $.tmpl('#tmpl_makemoney');
    slide_in(html, rN2, '.frame', false);
}

function n3Page(u){
    $('#footer_nav li').each(function(){
        this.className = (this.className.substr(0,6)=='nav_n3')?'nav_n3_now link':this.className.replace('_now','');
    });
    show_tab();
    var html = $.tmpl('#tmpl_discount');
    slide_in(html, rN3, '.frame', false);
}

function n4Page(u){
    $('#footer_nav li').each(function(){
        this.className = (this.className.substr(0,6)=='nav_n4')?'nav_n4_now link':this.className.replace('_now','');
    });
    show_tab();
    var html = $.tmpl('#tmpl_cost');
    slide_in(html, rN4, '.frame', false);
}

function n5Page(u){
    $('#footer_nav li').each(function(){
        this.className = (this.className.substr(0,6)=='nav_n5')?'nav_n5_now link':this.className.replace('_now','');
    });
    show_tab();
    var html = $.tmpl('#tmpl_setting');
    slide_in(html, rN5, '.frame', false);
}
